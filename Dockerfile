FROM debian:testing-slim
ENV LC_ALL=C.UTF-8
RUN apt-get update && apt-get install --yes --no-install-recommends \
  cabal-install \
  libghc-hakyll-dev \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /usr/share/doc/* /usr/share/man/*